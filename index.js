// Import vue components
import FunctionalCalendar from './src/components/FunctionalCalendar.vue'

// Creating a module value for Vue.use ()
const FunctionalCalendarPlugin = {
  install(Vue, options = []) {
    Vue.prototype.$getOptions = function() {
      return options
    }

    Vue.component('FunctionalCalendar', FunctionalCalendar)
  },
  FunctionalCalendar
}

// Export components for use as a module (npm / webpack / etc.)
export default FunctionalCalendarPlugin
export { FunctionalCalendar }
